# -*- encoding: utf-8 -*-

import socket
import time
from os import curdir, sep
import os
import cgi


def create_dir_html(path):
    if path[0]=='/':
        path = path[1:]
    directory = os.listdir(path)
    splitedpath = path.split('/')
    html = ("<html><body><ul>\n")
    for filename in directory:
        dir=''
        if len(splitedpath) > 1:
            for i in splitedpath:
                if not (str(i).__contains__('web')):
                    dir = i+sep
            html += ('<li><a href="%s">%s</a></li>\n' % (cgi.escape(dir+filename, True), cgi.escape(filename)))
        else:
            html += ('<li><a href="%s">%s</a></li>\n' % (cgi.escape(filename, True), cgi.escape(filename)))
    html +=("</ul></body></html>\n")
    return html

def create_html(path):
    fileName, fileExtension = os.path.splitext(path)
    if fileExtension =='':
        return create_dir_html(fileName)
    else:
        if path[0]=='/':
            path = path[1:]

        if fileExtension in ('.txt', '.html', '.jpg', '.png'):
            html = "<html><body>\n"
            html += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>"
            if fileExtension in (".txt", ".html"):
                txt=open(path).read()
                html+=txt
            if fileExtension in (".jpg", ".png"):
                data_uri = open(path, 'rb').read().encode('base64').replace('\n', '')
                img_tag = '<img src="data:image/png;base64,{0}">'.format(data_uri)
                html+= img_tag
            html+="</body></html>"
        else:
            html = ''
        return html

def http_serve(server_socket):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        request = ''
        try:
            while True:
                data = connection.recv(1024)
                request+=data
                if str(data).endswith('\n') or not data: break
            if request:
                html = ''
                req = request.split("\r\n")
                gethttp = req[0].split(' ')
                header = gethttp[2] +" 200 OK"
                header1 = gethttp[2] +" 404 ERROR"
		print gethttp
                if gethttp[0].upper()== 'GET' and (gethttp[2].upper() == "HTTP/1.1" or gethttp[2].upper() == "HTTP/1.0"):
                    if gethttp[1] == "/":
                        html = create_dir_html(curdir+sep+'web')
                    else:
                        html = create_html(curdir+sep+'web'+sep+gethttp[1])
                        if html == '':
                            raise Exception("Plik nie obsługiwany!")
                    odp = header+ "\r\nDate: " + str(time.ctime())  + "\r\n\r\n" + html
                else :
                    html = "<html><body>\n"
                    html += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>"
                    html += "Protokół inny niż HTTP albo metoda nie GET"
                    html += "</body></html>"
                    odp = header1 +"\r\n\r\n"+ html

                # Wysłanie zawartości strony

                connection.sendall(odp)
        except:
            html = "<html><body>\n"
            html += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>"
            html += "Page Not Found"
            html += "</body></html>"
            connection.sendall(header1+"\r\n\r\n"+html)
        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('localhost', 4444)
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)


try:
    print "dzia³a"
    http_serve(server_socket)

finally:
    server_socket.close()



