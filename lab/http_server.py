# -*- encoding: utf-8 -*-

import socket
import email.utils
import time

def http_serve(server_socket, html):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        request = ''
        try:
            # Odebranie żądania
            while True:
                data = connection.recv(1024)
                request += data
                print request

                if str(data) == "\r\n" or len(request)>24:
                    break
            if request:

                req = request.split("\r\n")
                print req[0]
                gethttp = req[0].split(' ')
                print gethttp[1]
                if gethttp[0].upper()== 'GET' and gethttp[2].upper() == "HTTP/1.1":
                    #gmtdate = time.gmtime()
                    #date = email.utils.formatdate(time.gmtime(), usegmt = True)
                    #date = time.gmtime()
                    print "Odebrano:"
                    print request
                    header = "HTTP/1.1 200 OK"
                    #print html
                    odp = header+ "\r\nDate: " + str(time.ctime())  + "\r\n\r\n" + html
                else :
                    header = "HTTP/1.1 401 ERROR"
                    odp = header +"\r\n"+ "Błąd"

                # Wysłanie zawartości strony
                connection.sendall(odp)

        finally:
            # Zamknięcie połączenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('localhost', 4444)  # TODO: zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

html = open('web/web_page.html').read()

try:
    http_serve(server_socket, html)

finally:
    server_socket.close()

